.. NAO Robot Documentation documentation master file, created by
   sphinx-quickstart on Fri Jan 24 15:54:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*************************************
Welcome to NAO Robot's documentation!
*************************************

This documentation was created in order to help with the usage of the NAO Robot.


Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   members
   gettingStarted
   resources
   basicCommands
   installedApps
   creatingApps


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
