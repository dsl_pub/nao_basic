****************************************
Getting Started
****************************************

##########################
Creating a GitLab account.
##########################

In order to create a GitLab account, `click this link`_ and follow the on screen prompt.

Once your account is created you will have to decide on what program to use in order to SSH into GitLab. I recommend either Git for Windows, or using Windows Subsystem for Linux.


###############
Git for Windows
###############

Git for Windows is a program that allows you to use Git commands using a terminal.

**Installing Git for Windows**

        1. `Download Git for Windows`_.

        2. Run the installer follow the prompts.

        3. Choose your text editor, Vim is the default, but you can use whatever text editor you prefer, some text editors even have plugins for git commands you can install.

        4. You're done installng Git for Windows. 

###########################
Windows Subsystem for Linux
###########################

Windows Subsystem for Linux (from here on abbreviated WSL) is a developer option in Windows 10 that allows you run an instance of linux inside of Windows. It is extremely similar to running a virtual machine with one exception, WSL does not have native gui support. This means you only have access to a terminal and some basic text editors. While this seems like a huge drawback, WSL can be a lightweight and fast alternative to running a virtual machine.  


**Installing Windows Subsystem for Linux**

        1. Turn on "Developer mode" in Windows by going to Settings > Update and Security.

        2. Once you have enabled developer mode, you can go on the Microsoft Store and install whatever version of Linux you prefer, for example Ubuntu.

        3. After you complete the download, you can start a Linux terminal by selecting your selected Linux distribution from the windows Start Menu.

#####################
Generating an SSH key
#####################

Now that you have a platform on which to use git, you need an SSH key so that GitLab trusts your connection.

        1. First, decide what key type to use. For this example, we will use a ED25519 key. The classification ED25519 just describes what protocol was used to generate it. ED25519 keys are the most secure, and they also perform better compared to other key types. 

        2. Start a terminal on your preferred platform.
       
        3. Generate a new key pair using the following command
                ::
           
                        ssh-keygen -t ed25519 -C “email@example.com”

                        NOTICE: -C indicates you are adding a comment. 
                        Use this to differentiate between multiple keys.

        4. Approve the default path for saving keys by pressing “enter”
                  
        5. You will now be prompted to add a password. It is recommended to do so, but not necessary.
        
        6. Your SSH key is created!

        7. Copy your SSH key to your clipboard to help with the next step. 
                ::
                
                        If you used WSL, then you need to enter this command into the terminal: 
                        
                        xclip -sel clip < ~/.ssh/id_ed25519.pub

                        
                ::
                 
                        If you used Git for Windows, then you need to enter this command into the terminal: 
                        
                        cat ~/.ssh/id_ed25519.pub | clip

        8. Now you must add your newly generated key to GitLab. To do this, start by going to your GitLab dashboard.

        9. Click the drop-down menu in the top right, and select “Settings”

        10. In the left menu list, select “SSH Keys”

        11. Copy your key into the text box provided, double check the key’s name, and then add it to your account. You can also manage all other SSH keys directly from this window.

        12. Your SSH key was added! GitLab will now trust your machine when you connect.

###################################################
Creating your first project 
###################################################

        1. Now that you can connect to GitLab, you need a project to experiment in.

        2. Go to your GitLab dashboard and click on the button that says “New Project”
        
        3. Title your project and give it an appropriate description, for example “Example Project”.
        
        4. Decide if you want your project to be Public or Private
        
        5. I recommend selecting the “Initialize repository with a README” because this will allow you to clone the repository as soon as possible.
        
        6. When you are satisfied with everything you can click “Create Project” at the bottom of the page.

        7. You have now created your first project on GitLab

###########################################
Adding GitLab Credentials to your terminal.
###########################################

        Now that you have a project, you need to add some basic information to your Git platform so that GitLab knows who you are when you try to connect.
                
        1. Open a terminal on your preferred platform. 

        2. Add your username by executing this command in the terminal with your information.

                ::
                
                        git config --global user.name "YOUR_USERNAME"
                        
        3. Add your email by executing this command in the terminal with your information.
        
                ::
                
                        git config --global user.email "your_email_address@example.com"
        4. You can double check that the information is correct by executing the following command
                   
                ::
                
                        git config --global user.name
                        git config --global user.email
                        These commands report what username and email are registered.

        5. You are done adding your credentials.

#######
Cloning
#######

Cloning is creating a local version of a repository downloaded to your machine so you can work on it WITHOUT it affecting the GitLab repository. This means all the work you do after cloning will not have an effect until you “push” it with git. 

        1. Cloning a project can be done via SSH using your Git terminal.

        2. Go to your GitLab Dashboard.

        2. Select the project you want to clone. In this case it will be the “Example Project”
        
        3.      Below your project title to the right there should be a blue button that says “Clone”
                       
        4.      Click the copy button for the SSH version of the link, or copy it using your preferred method. If you use the HTTPS version, you need to follow a different process.
                        
        5.      Open a Git terminal where you want to download your project, I recommend making a separate folder for all your GitLab projects.

        6.      Use the following command. NOTICE : Your command will use your own username and the link you copied from GitLab, this means it will be similar, but not exactly the same as the example provided
                        
                        ::
                
                                git clone git@gitlab.com:YourUsername/example-project.git

        7.      You have now successfully cloned a GitLab project.


##########
Committing
##########

Committing is deciding on which changes, to your project, you want to make public. This is the precursor to pushing. 

        1. First you must decide what to commit
                   
                        ::
                        
                                If you are committing **all** of your local files, you can use this command:
                                
                                        git add .

                        ::

                                If you are only committing a certain file of directory within your repository,
                                use this command:
                                
                                        git add <file-name OR folder-name>


         2. Now that you have added the files you want to update you have to commit the changes.

         3. Use this command to commit all recently added changes.
                     
                ::
                        
                        git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
                                
                        NOTICE: Anything inside the quotes is akin to a comment in coding.
                        It has no effect other than to keep track of what was done in a commit.
                        You can use this to keep track of what you have done. 

#######
Pushing
#######

Pushing makes your changes final and updates everything you’ve changed in the GitLab repository. This means that after you push, GitLab will show your updated files, any cloning after the push has the new files, and most importantly, if you work on it on any other machine that has not been updated you will be working on an outdated and incorrect version of the project.

        1. When you first clone a repository a remote, typically called origin, is created. A remote allows you to shorthand the URL you are pushing to. To double check you are pushing to the correct repository, in a terminal inside your local repository files execute this command.
                   
                ::
        
                        git remote –verbose
                        
                        “—verbose” makes it so that you also see the URL you are pushing to.

        2.  Once you are sure you are pushing to the correct GitLab repository, all you need to do is execute the following command:
                    
                ::
        
                        git push origin master
                        
                        “origin” is the remote, it tells the terminal what URL to push to.
                        
                        "master” tells the terminal what branch to push your changes to.
                        If you have multiple branches you can choose to send changes to a specific branch.

        3. You will receive confirmation that your changes were pushed, and what files were changed.
                                                                                                                                                                                            4. Congratulations, you’ve updated your repository. This can be done from any computer or program that can SSH into GitLab and has the proper credentials.

#######
Forking
#######

Forking is creating your own copy of a project. This version will then be a different project, which you can tweak to your specific use case or you merge with the original repository to improve the original.

        1. To fork a project, you must first go to its GitLab page.

        2. On the right-hand side of the screen beside the project name, there should be a button that says “Fork” it will also show how many others have forked the specific project you are looking at.

        3. Once you click it, you will be taken to a screen to select what name-space to add it to. If you have multiple name-spaces choose which name you want to work under, otherwise just click your name-space.

        4. GitLab will now fork the project. This may take a while for larger projects.

        5. Once it is done, your new project will be added to your GitLab dashboard.
                   
                ::
        
                        Notice: To start working on your newly forked project, you need to clone it. 
                        This will download all files to your machine.

        6. You have successfully forked a project. 



.. _click this link: http://gitlab.com/users/sign_up
.. _Download Git for Windows: https://git-scm.com/download/win
