*****************
What can NAO do?
*****************

For an exhaustive list of basic voice commands, use the `Softbank Robotics - What can i say to NAO`_ page.

Below, some of the more interesting ones are highlighted. 

Basic Communication
###################

Nao knows a broad range of general communication prompts. Some of these are: 

        Hello.

        How are you?
        
        What's your name?
        
        What time is it?
        
        What is your exact battery level.

Basic Commands
##############

Nao comes preprogrammed with a few commands, some make him move, others control his settings.

        Speak louder/softer.
        
        Speak Chinese **Warning - You must speak Chinese to change him back**
        
        Stop talking.
        
        Can you shutdown/restart/go to sleep?
        
        Can you stand up?
        
        Can you sit down?
        
        Can you raise your arms?
        
        Hands up!
        
        Close/open your hand.
        
        Can you fly? Nao knows his limits, and he'll tell you. 


Quirky Questions
################

These prompt interesting replies from Nao, and some might even teach you something you didn't know!

        Are you human?
        
        Are robots dangerous?
        
        Will robots replace humans?
        
        Who is Asimov?
        
        Do you know the laws of robotics?
        

Musical Mastery
###############

Nao knows quite a few songs, just tell him to sing, followed by the song title and he will happly share his knowledge with you. 

Sing: 

        Ring around the Rosie
        
        Mary had a little lamb.
        
        Row, row, row, your boat.
        
        Do Re Mi
        
        Hello Robots
        
        the ABC song

There are many more commands that Nao understands. You can also download more, or even program your own. The possibilities are infinite. 



.. _softbank robotics - what can i say to nao: http://doc.aldebaran.com/2-8/family/nao_user_guide/basic_channel_conversation_nao.html#bchannel-text-nao
