*********
Resources
*********

Online Documentation
####################

You can always use the `SoftBank Robotics Official Documentation`_.


Factory Reset your NAO
######################

To factory reset your NAO robot, follow `this`_ guide by RobotLab.

Factory resetting your Nao robot will **delete all data** currently stored inside the Nao robot.
This is irreversible.
Factory Resetting your Nao will also reset any settings or passwords you have created


.. _SoftBank Robotics Official Documentation: http://doc.aldebaran.com/2-8/family/nao_user_guide/introduction_nao.html
.. _this: https://www.robotlab.com/support/factory-reset-nao-robot-
