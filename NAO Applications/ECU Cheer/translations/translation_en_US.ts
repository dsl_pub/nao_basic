<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>WAAHAAHAHAHA</source>
            <comment>Text</comment>
            <translation type="obsolete">WAAHAAHAHAHA</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Cheer for East Car'lina</source>
            <comment>Text</comment>
            <translation type="obsolete">Cheer for East Car'lina</translation>
        </message>
        <message>
            <source>Cheer for East Car'lyenah</source>
            <comment>Text</comment>
            <translation type="obsolete">Cheer for East Car'lyenah</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Cheer for East Carolina</source>
            <comment>Text</comment>
            <translation type="unfinished">Cheer for East Carolina</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Cheer on for Old EC</source>
            <comment>Text</comment>
            <translation type="obsolete">Cheer on for Old EC</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Cheer for Old EC</source>
            <comment>Text</comment>
            <translation type="unfinished">Cheer for Old EC</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (2)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Loyal and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="obsolete">Loyal and bold, we're the purple and gold. </translation>
        </message>
        <message>
            <source>LOYAL and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="obsolete">LOYAL and bold, we're the purple and gold. </translation>
        </message>
        <message>
            <source>loyal and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="obsolete">loyal and bold, we're the purple and gold. </translation>
        </message>
        <message>
            <source>lloyal and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="obsolete">lloyal and bold, we're the purple and gold. </translation>
        </message>
        <message>
            <source>loyale and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="obsolete">loyale and bold, we're the purple and gold. </translation>
        </message>
        <message>
            <source>and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="obsolete">and bold, we're the purple and gold. </translation>
        </message>
        <message>
            <source>loyale, and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="obsolete">loyale, and bold, we're the purple and gold. </translation>
        </message>
        <message>
            <source>loyal, and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="obsolete">loyal, and bold, we're the purple and gold. </translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>loyal,, and bold, we're the purple and gold. </source>
            <comment>Text</comment>
            <translation type="unfinished">loyal,, and bold, we're the purple and gold. </translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (3)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>We're the pirates of E.C.U.</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of E.C.U.</translation>
        </message>
        <message>
            <source>We're the pirates of Ee.C.U.</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of Ee.C.U.</translation>
        </message>
        <message>
            <source>We're the pirates of e.C.U.</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of e.C.U.</translation>
        </message>
        <message>
            <source>We're the pirates of e c u</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of e c u</translation>
        </message>
        <message>
            <source>We're the pirates of i c u</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of i c u</translation>
        </message>
        <message>
            <source>We're the pirates of ee c u</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of ee c u</translation>
        </message>
        <message>
            <source>We're the pirates of  ecu</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of  ecu</translation>
        </message>
        <message>
            <source>We're the pirates of e,cu.</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of e,cu.</translation>
        </message>
        <message>
            <source>We're the pirates of e,c,u.</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of e,c,u.</translation>
        </message>
        <message>
            <source>We're the pirates of e.c.u.</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of e.c.u.</translation>
        </message>
        <message>
            <source>We're the pirates of e,c.u.</source>
            <comment>Text</comment>
            <translation type="obsolete">We're the pirates of e,c.u.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>We're the pirates of E,C,U.</source>
            <comment>Text</comment>
            <translation type="unfinished">We're the pirates of E,C,U.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (4)</name>
        <message>
            <source>loyale</source>
            <comment>Text</comment>
            <translation type="obsolete">loyale</translation>
        </message>
        <message>
            <source>loyale &lt;loyall&gt;</source>
            <comment>Text</comment>
            <translation type="obsolete">loyale &lt;loyall&gt;</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>loyal,</source>
            <comment>Text</comment>
            <translation type="unfinished">loyal,</translation>
        </message>
    </context>
</TS>
