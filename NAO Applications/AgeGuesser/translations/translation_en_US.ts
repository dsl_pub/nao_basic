<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>I'm sorry, i couldn't quite tell. Let me have another chance!</source>
            <comment>Text</comment>
            <translation type="obsolete">I'm sorry, i couldn't quite tell. Let me have another chance!</translation>
        </message>
    </context>
</TS>
