<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Dab/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>I love to dab.</source>
            <comment>Text</comment>
            <translation type="obsolete">I love to dab.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Dab/Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I love to dab.</source>
            <comment>Text</comment>
            <translation type="unfinished">I love to dab.</translation>
        </message>
    </context>
</TS>
